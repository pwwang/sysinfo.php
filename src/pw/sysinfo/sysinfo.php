<?php
namespace pw\Sysinfo;

class Sysinfo {
	
	const OS_WIN     = 0;
	const OS_UNIX    = 1;
	const OS_OSX     = 2;
	const OS_CYGWIN  = 3;
	const OS_UNKNOWN = 4;
	
	static private $os        = null;
	static private $osname    = null;
	static private $machtype  = null;
	static private $phpbit    = null;
	static private $parser    = null;
	
	static public function os () {
		if (!is_null(self::$os)) return self::$os;
		
		self::unames ();
		return self::$os;
	}
	
	static public function osname () {
		if (!is_null(self::$osname)) return self::$osname;
		
		self::unames ();
		return self::$osname;
	}
	
	static public function machtype () {
		if (!is_null(self::$machtype)) return self::$machtype;
		self::$machtype = self::unameout ('m');
		return self::$machtype;
	}
	
	static public function phpbit () {
		if (!is_null(self::$phpbit)) return self::$phpbit;
		self::$phpbit = PHP_INT_SIZE * 8;
		return self::$phpbit;
	}
	
	static public function __callStatic ($name, $arguments) {
		if (is_null(self::$parser))
			self::$parser = new SysinfoParser(self::osname());
		return self::$parser->$name;
	}
	
	static private function unameout ($flag) {
		$ret = trim (strtolower(php_uname ($flag)));
		return $ret;
	}
	
	static private function startswith ($str, $start) {
		$sslen = strlen ($start);
		$ss    = substr($str, 0, $sslen);
		return $ss === $start;
	}
	
	static private function unames () {
		$uname = self::unameout ('s');
		switch (true) {
			case self::startswith($uname, 'windows'):
				self::$os = self::OS_WIN;
				self::$osname = "Windows";
				break;
			case self::startswith($uname, 'cygwin'):
				self::$os = self::OS_CYGWIN;
				self::$osname = "Cygwin";
				break;
			case self::startswith($uname, 'darwin'):
				self::$os = self::OS_OSX;
				self::$osname = "OSX";
				break;
			case self::startswith($uname, 'linux'):
				self::$os = self::OS_UNIX;
				self::$osname = "Linux";
				break;
			case self::startswith($uname, 'freebsd'):
				self::$os = self::OS_UNIX;
				self::$osname = "FreeBSD";
				break;
			default:
				self::$os = self::OS_UNKNOWN;
				self::$osname = "Unknown";
				break;				
		}
	}
	
}
