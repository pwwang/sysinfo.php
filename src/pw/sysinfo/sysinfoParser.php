<?php
namespace pw\Sysinfo;

class SysinfoParser {
	
	private $info;
	
	public function __construct ($osname) {
		switch ($osname) {
			case 'Windows':
			case 'Cygwin':
				$this->initWindows ();
				break;
			case 'Linux':
				$this->initLinux ();
				break;
			case 'FreeBSD':
				$this->initFreeBSD ();
				break;
			case 'OSX':
				$this->initOSX ();
				break;
			default:
				throw new \Exception ("Have no idea how to parse sysinfo for $osname");
		}
	}
	
	public function __get ($name) {
		if (!isset($this->info[$name]))
			throw new \RuntimeException ("No property named $name for " . __CLASS__);
		return $this->info [$name];
	}
	
	private function initWindows () {
		
		$this->info ['cpucount'] = $this->cmdout ("wmic cpu get NumberOfCores");
		$this->info ['cpuname']  = $this->cmdout ("wmic cpu get Name");
		$this->info ['cpuspeed'] = $this->cmdout ("wmic cpu get MaxClockSpeed");
		$this->info ['memtotal'] = array_sum ($this->cmdout ("wmic memorychip get capacity", true));
		
	}
	
	private function initFreeBSD () {

        $dmesgPath = '/var/run/dmesg.boot';
        
        if ( !is_file( $dmesgPath ) ) 
			throw new \RuntimeException ('System information file does not exist: ' . $dmesgPath);
        
        $fileLines = file( $dmesgPath );
		$this->info ['cpucount'] = 1;
        foreach ( $fileLines as $line ) {
			
            if ( substr( $line, 0, 3 ) == 'CPU' ) {
                $system = trim( substr( $line, 4, strlen( $line ) - 4 ) );
                $cpu = null;
                // we should have line like "CPU: AMD Duron(tm)  (1800.07-MHz 686-class CPU)" parse it.
                if ( preg_match( "#^(.+)\\((.+)-(MHz) +([^)]+)\\)#", $system, $matches ) ) {
                    $system = trim( $matches[1] ) . ' (' . trim( $matches[4] ) . ')';
                    $cpu = $matches[2];
                    $cpuunit = $matches[3];
                }
                $this->info ['cpuspeed'] = $cpu;
                $this->info ['cpuname']  = $system;
				continue;
            }
			
            if ( substr( $line, 0, 44 ) == 'FreeBSD/SMP: Multiprocessor System Detected:' ) {
                $multiCpu = trim( substr( $line, 44, strlen( $line ) - 44 ) );
                unset( $matches );
                // we should have line like "FreeBSD/SMP: Multiprocessor System Detected: 4 CPUs" parse it.
                if ( preg_match( "#^([0-9]+).+#", $multiCpu, $matches ) ) {
                   $this->info ['cpucount'] = (int)$matches[1];
                }
				continue;
            }
			
            if ( substr( $line, 0, 11 ) == 'real memory' ) {
                $mem = trim( substr( $line, 12, strlen( $line ) - 12 ) );
                $memBytes = $mem;
                if ( preg_match( "#^= *([0-9]+)#", $mem, $matches ) ) {
                    $memBytes = $matches[1];
                }
                $this->info ['memtotal'] = $memBytes;
				continue;
            }
        }
	}
	
	private function initLinux () {
		
		$cpuinfoPath = '/proc/cpuinfo';
        $meminfoPath = '/proc/meminfo';
		
		if (!is_file($cpuinfoPath))
			throw new \RuntimeException ('Cannot find CPU info file: ' . $cpuinfoPath);
        
		if (!is_file($meminfoPath))
			throw new \RuntimeException ('Cannot find memory info file: ' . $meminfoPath);
		
        $this->info ['cpucount'] = 0;
        $fileLines = file( $cpuinfoPath );
        foreach ( $fileLines as $line ) {
            if ( substr( $line, 0, 7 ) == 'cpu MHz' ) {
                $cpu = trim( substr( $line, 11, strlen( $line ) - 11 ) );
                $this->info ['cpuspeed'] = $cpu;
                $this->info ['cpucount'] ++;
				
				continue;
            }
			
            if ( substr( $line, 0, 10 ) == 'model name' ) {
                $system = trim( substr( $line, 13, strlen( $line ) - 13 ) );
                $this->info ['cpuname'] = $system;
				
				continue;
            }
        }
		
        $fileLines = file( $meminfoPath );
        foreach ( $fileLines as $line ) {
            if ( substr( $line, 0, 8 ) == 'MemTotal' ) {
                $mem = trim( substr( $line, 11, strlen( $line ) - 11 ) );
                $memBytes = $mem;
                if ( preg_match( "#^([0-9]+) *([a-zA-Z]+)#", $mem, $matches ) ) {
                    $memBytes = (int)$matches[1];
                    $unit = strtolower( $matches[2] );
                    if ( $unit == 'kb' ) {
                        $memBytes *= 1024;
                    }
                    else if ( $unit == 'mb' ) {
                        $memBytes *= 1024*1024;
                    }
                    else if ( $unit == 'gb' ) {
                        $memBytes *= 1024*1024*1024;
                    }
                }
                else {
                    $memBytes = (int)$memBytes;
                }
                $this->info ['memtotal'] = $memBytes;
				break;
            }
        }
	}
	
	private function initOSX () {
		$this->info ['cpucount'] = $this->cmdout ("sysctl -n hw.ncpu", false, false);
		$this->info ['cpuname']  = $this->cmdout ("sysctl -n machdep.cpu.brand_string", false, false);
		$this->info ['cpuspeed'] = $this->cmdout ("sysctl -n hw.cpufrequency", false, false);
		$this->info ['memtotal'] = $this->cmdout ("sysctl -n hw.physmem", false, false);
	}
	
	private function cmdout ($cmd, $retarr = false, $remove1st = true) {
		$output = shell_exec ($cmd);
		$outs   = explode( "\n", trim($output));
		//if (!isset($outs[1]))
		//	throw new \RuntimeException ('Failed to run cmd "'. $cmd .'"');
		
		if ($remove1st) array_shift($outs);
		return $retarr ? $outs : $outs[0];
	}
	
}
