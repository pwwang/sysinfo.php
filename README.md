# sysinfo.php
Get system information

## Install
Install via [Composer](http://getcomposer.org/):
```json
"require": {
    "pwwang/sysinfo.php": "*"
}
```
or 
`composer require pwwang/sysinfo.php`

## Usage
```php
use pw\sysinfo\sysinfo;

echo "OS:        " . sysinfo::os() . PHP_EOL;
// === sysinfo::OS_CYGWIN
echo "OS NAME:   " . sysinfo::osname() . PHP_EOL;
echo "MACH TYPE: " . sysinfo::machtype() . PHP_EOL;
echo "PHP BIT:   " . sysinfo::phpbit() . PHP_EOL;
echo "CPU COUNT: " . sysinfo::cpucount() . PHP_EOL;
echo "CPU NAME:  " . sysinfo::cpuname() . PHP_EOL;
echo "CPU SPEED: " . sysinfo::cpuspeed() . PHP_EOL;
echo "MEM TOTAL: " . sysinfo::memtotal() . PHP_EOL;

/*
OS:        3
OS NAME:   Cygwin
MACH TYPE: x86_64
PHP BIT:   64
CPU COUNT: 4
CPU NAME:  Intel(R) Core(TM) i7-2600 CPU @ 3.40GHz
CPU SPEED: 3401
MEM TOTAL: 17179869184
*/
```
