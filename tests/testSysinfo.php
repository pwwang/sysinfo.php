<?php
require_once __DIR__ . "/../vendor/autoload.php";
use \pw\sysinfo\sysinfo;

class testSysinfo extends PHPUnit_Framework_TestCase {
	
	public function testIt () {
		echo "\nOS:        " . sysinfo::os() . PHP_EOL;
		echo "OS NAME:   " . sysinfo::osname() . PHP_EOL;
		echo "MACH TYPE: " . sysinfo::machtype() . PHP_EOL;
		echo "PHP BIT:   " . sysinfo::phpbit() . PHP_EOL;
		echo "CPU COUNT: " . sysinfo::cpucount() . PHP_EOL;
		echo "CPU NAME:  " . sysinfo::cpuname() . PHP_EOL;
		echo "CPU SPEED: " . sysinfo::cpuspeed() . PHP_EOL;
		echo "MEM TOTAL: " . sysinfo::memtotal() . PHP_EOL;
		
		//$this->assertEquals (1,1);
	}
}